package assignment1;

import java.util.Scanner;

public class Triangle {

	private static String equilateral(){
		return "This is an equalateral triangle";
	}
	
	private static String isosceles(){
		return "This is an isosceles triangle";
	}
	
	private static String scalene(){
		return "This is a scalene triangle";
	}
	private static String findTriangleType(int a, int b, int c){
		if(isTriangle(a, b, c)){
			if(a == b){
				if( b == c){
					return equilateral();
				}
				else{
					return isosceles();
				}
			}
			else if (b == c){
				return isosceles();
			}
			else if( a == c){
				return isosceles();
			}
			else{
				return scalene();
			}
		}
		else{
			return "This is not a triangle";
		}
	}
	private static boolean isTriangle(int a, int b, int c){
		if(a+b >= c){
			return true;
		}
		else{
			return false;
		}
	}
	private static boolean isInvalidNumber(int number){
		if(number > 0){
			return false;
		}
		else{
			return true;
		}
	}
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int a = -1;
		int b = -1;
		int c = -1;
		while(isInvalidNumber(a)){
			System.out.println("Please enter the first number for the triangle.");
			a = scanner.nextInt();
		}
		while(isInvalidNumber(b)){
			System.out.println("Please enter the second number for the triangle");
			b = scanner.nextInt();
		}
		while(isInvalidNumber(c)){
			System.out.println("Please enter the last number for the triangle");
			c = scanner.nextInt();
		}
		
		System.out.println(findTriangleType(a, b, c));
	}

}
